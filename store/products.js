export const state = () => ({
  productsCart: [],
})


export const mutations = {
  addProduct(state, product) {
      console.log(state)
      state.productsCart.push(product)
  },
}


export const actions = {
  addProduct(context, product) {
      const sliceProduct = sliceProduct(product)
      context.commit(sliceProduct)
  },
}

export const getters = {
  getProducts: (state) => {
      return state.productsCart
  },
}